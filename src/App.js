import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Form from './Form';
import Results from './Results'

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
        firstName: '',
        lastName: '',
        street1: '',
        city: '',
        state: '',
        zip: '',
        DOB: '',
        gender: '',
        doctors: []
    };
    this.getIndividualPatient = this.getIndividualPatient.bind(this);
  }

  getPatients(text){
    console.log(text)
    fetch("https://api.hsx.io/patients", {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': "8lLXvqPsHP8alYCZRTPNT30g2Ch1hR6O3SN9QR75"
      }})
    .then((response) => response.json())
    .then((responseJson) => console.log(responseJson))
  };

    getIndividualPatient(id){
      fetch("https://api.hsx.io/patients/" + id, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': "8lLXvqPsHP8alYCZRTPNT30g2Ch1hR6O3SN9QR75"
        }})
        .then((response) => response.json())
        .then((responseJson) => {
          const patientFirstName = responseJson.name.first;
          const patientLastName = responseJson.name.last;
          const street1 = responseJson.homeAddress.street1;
          const city = responseJson.homeAddress.locality;
          const state = responseJson.homeAddress.region;
          const zip = responseJson.homeAddress.postal;
          const DOB = "DOB: " + responseJson.dob;
          const gender = "Gender: " + responseJson.gender.code;
          const doctors = responseJson.patientProviders.map( (doctor) => {
            this.state.doctors.push(doctor);
          })
          this.setState({firstName: patientFirstName, lastName: patientLastName, street1: street1, city: city, state: state, zip: zip, DOB: DOB, gender: gender })
        })
        console.log(this.state.doctors)
    }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Welcome to our Predictive Health Care App</h1>
        </header>
        <Form getIndividualPatient={this.getIndividualPatient}/>
        <Results firstName={this.state.firstName} lastName={this.state.lastName} street={this.state.street1} city={this.state.city} zip={this.state.zip} dob={this.state.DOB} gender={this.state.gender}/>
      </div>
    );
  }
}

export default App;
