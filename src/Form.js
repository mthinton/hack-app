import React from 'react'

const Form = ({getIndividualPatient}) => (
  <form onSubmit={(event) => {
    event.preventDefault();

    const userInput = event.target.userInput.value;

    getIndividualPatient(userInput)

    event.target.userInput.value = ''

  }}>
    <label>Enter your info here</label>
    <input type="text" name="userInput"/>
  </form>
)

export default Form
