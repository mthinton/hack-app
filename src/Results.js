import React from 'react'
import './App.css'

const Results = (props) => {
  console.log(props)
  return (
  <div>
    <h1>
      {props.firstName} {props.lastName}
    </h1>
    <h4 className="dashboardContainer">
      {props.street}<br></br>
      {props.city}<br></br>
      {props.zip}<br></br>
      {props.dob}<br></br>
    </h4>
    <h4 className="dashboardContainer2">
      {props.gender}<br></br>
    </h4>
  </div>
)
}

export default Results
